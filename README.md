# Django Task Management Project

This Django project implements a task management system with CRUD (Create, Read, Update, Delete) operations for tasks.

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository to your local machine:

    ```
    git clone https://gitlab.com/balaji1723/task-management.git
    ```

2. Install the required dependencies:

    ```
    pip install -r requirements.txt
    ```

3. Apply migrations to create the necessary database tables:

    ```
    python manage.py migrate
    ```

4. Run the development server:

    ```
    python manage.py runserver
    ```

5. Access the application in your web browser at [http://localhost:8000/](http://localhost:8000/).

## Functionality

This project includes the following functionality:

- **Task Model**: Defines a Task model with fields for title, description, created_at, due_date, completed status, user assignment, and priority level.

- **CRUD Operations**: Implements CRUD operations for managing tasks. Users can perform the following actions:
    - Create new tasks
    - Read/view existing tasks
    - Update existing tasks
    - Delete tasks

## Technologies Used

- Django: A high-level Python web framework for rapid development.
- SQLite: A lightweight relational database used for storing task data.
- HTML/CSS: Frontend languages for rendering web pages and styling.

## Project Structure

- `task_management/`: Main Django project directory.
- `tasks/`: Django app directory containing the Task model and related views, templates, and forms.
- `templates/`: HTML templates for rendering the user interface.
- `static/`: Static files such as CSS, JavaScript, and images.

# Entity-Relationship Diagram (ERD) between Task and User Entities

This project involves managing tasks assigned to users. Below is the ERD illustrating the relationship between the "Task" and "User" entities.

## ERD Diagram

 _________           __________
|  User   |         |  Task    |
|---------|         |----------|
| user_id |<--------| task_id  |
| name    |         | title    |
| email   |         | details  |
|         |         | status   |
 ¯¯¯¯¯¯¯¯¯           ¯¯¯¯¯¯¯¯¯¯

- **User Entity Attributes**:
  - `user_id`: Primary key uniquely identifying each user.
  - `name`: Name of the user.
  - `email`: Email address of the user.

- **Task Entity Attributes**:
  - `task_id`: Primary key uniquely identifying each task.
  - `title`: Title of the task.
  - `details`: Additional details or description of the task.
  - `priority`: Current status of the task.

- **Relationship**:
  - There exists a one-to-many relationship between "User" and "Task." This means one user can have multiple tasks assigned to them, while each task is associated with exactly one user.

This ERD serves as a visual representation of how tasks are linked to users in the system.


## Contributing

Contributions are welcome! If you have suggestions or would like to report any issues, please open an issue or submit a pull request.
