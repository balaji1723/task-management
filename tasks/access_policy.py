from rest_access_policy import AccessPolicy

class TaskAccessPolicy(AccessPolicy):
    statements = [
        {
            "action": ["create", "retrieve"],
            "principal": ["is_staff"],
            "effect": "allow"
        },
        {
            "action": ["create", "retrieve", "update", "partial_update", "destroy"],
            "principal": ["is_superuser"],
            "effect": "allow"
        },
    ]