from django.contrib import admin
from .models import Task

class TaskAdmin(admin.ModelAdmin):
    """
    For Task model to provide functionalities like filetr, search and display filelds.
    """
    list_display = ('title', 'priority', 'due_date', 'completed', 'user')
    list_filter = ('priority', 'due_date', 'user')
    search_fields = ('title',)

admin.site.register(Task, TaskAdmin)