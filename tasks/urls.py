from django.urls import path, include
from rest_framework import routers
from tasks.views import TaskViewSet, create_task_view

router = routers.DefaultRouter()
router.register(r'tasks', TaskViewSet)

urlpatterns = [
    path('api/', include(router.urls)),
    path('tasks/', create_task_view, name='create_task'),

]